# Colour test sheet
In this folder, there are a number of PDF files.  Most of these are solid colours, one of them is the colour wheel test chart used in the manuscript.  Source SVG files (produced using Inkscape) are also included.  For colours other than green, use ``green.svg`` and change the colour of the large rectangle.

Each of these PDF files was displayed in turn on a monitor (or printed out and placed on a stand).  Images were acquired by running:
```bash
python measure_colour_response.py --manual_illumination --additional_images colour_wheels
```
This will prompt you to display each of the colours (red, green, blue, black, white) in turn, then the colour wheels chart.
