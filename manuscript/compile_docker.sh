#!/bin/sh
if [[ `uname -s` == MINGW* ]];
then
    #echo "MINGW Detected"
    exec docker run --rm -i --net=none -v "`pwd | sed -E 's/^\/(.)\//\1:\//'`":/data blang/latex "$@"
else
    #echo "Not running in MINGW"
    exec docker run --rm -i --user="$(id -u):$(id -g)" --net=none -v "$PWD":/data blang/latex "$@"
fi
