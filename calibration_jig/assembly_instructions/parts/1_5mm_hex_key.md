# 1.5mm hex key

You will need either an appropriately-sized hex key ("Allen key" in British parlance) or an appropriate screwdriver to tighten your [M2 screws](m2x6mm_screws.md).  If you have a different head type, you may need a different hex key, or indeed a slotted or cross-head screwdriver.
