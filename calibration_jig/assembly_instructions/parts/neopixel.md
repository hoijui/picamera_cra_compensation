# NeoPixel

We use an RGB LED for illumination - specifically, a single [NeoPixel](https://learn.adafruit.com/adafruit-neopixel-uberguide).  We've used one LED cut out of a [30 LED per metre strip](https://www.mouser.co.uk/ProductDetail/Adafruit/2828?qs=sGAEpiMZZMu%252BmKbOcEVhFUBxNE2d1fkxcUmsA7T0pKO7i7IC5aqbFA%3D%3D).  There are plenty suppliers, official and unofficial, of these LEDs.  You can use any form factor - but the strip is nice, because it will fit neatly into the clamp.
