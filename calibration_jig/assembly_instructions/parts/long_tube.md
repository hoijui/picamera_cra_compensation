# Long tube
The long, ridged tube serves to collimage the light from the diffused LED, so it is incident on the camera module at a constant angle.

## Printing instructions
The long tube should be printed vertically, with the end that mounts the LED on the bottom; this should be the orientation in the STL file.  It does not require support material, but you may find that using a brim helps it to stick to the bed during printing.

## STL Files
[``illumination_tube.stl``](../../stl/illumination_tube.stl)

