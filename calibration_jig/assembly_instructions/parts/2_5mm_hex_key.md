# 2.5mm hex key

You will need either an appropriately-sized hex key ("Allen key" in British parlance) or an appropriate screwdriver to tighten your [M3 screws](m3x8mm_screws.md).  If you have a different head type, you may need a different hex key, or indeed a slotted or cross-head screwdriver.
