/*
This is a thin shim of plastic, that fits between the Raspberry Pi camera module and the
OpenFlexure Microscope optics module, to fine-tune the axial position of the camera.

This is useful when using the infinity-corrected optics module as an imaging system
for an object a few metres away from the camera.

(c) Richard Bowman, 2019
Released under CERN Open Hardware License

*/

shim_h=0.5;

intersection(){
    import("../stl/optics_picamera_2_rms_infinity_f50d13_LS65.stl");
    translate([0,0,-29.5]) cylinder(r=999,$fn=4, h=shim_h);
}