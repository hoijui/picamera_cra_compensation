.. Picamera raw analysis documentation master file, created by
   sphinx-quickstart on Fri Apr 24 13:57:55 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to the Picamera raw analysis documentation!
===================================================
.. automodule:: picam_raw_analysis
   :noindex:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   API


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
